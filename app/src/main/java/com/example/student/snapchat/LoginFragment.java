package com.example.student.snapchat;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

private EditText usernameField;
private EditText passwordField;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        Button button = (Button) view.findViewById(R.id.loginButton);
        usernameField = (EditText) view.findViewById(R.id.usernameField);
        passwordField = (EditText) view.findViewById(R.id.passwordField);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameField.getText().toString();
                String password = passwordField.getText().toString();

                Backendless.UserService.login(username, password, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Toast.makeText(getActivity(), "You logged in!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(), "Error logging in.", Toast.LENGTH_SHORT).show();
                    }
                },
                true);
            }
        });

       // String[] menuItems = {"Profile",
               // "Friends"};

      //  ListView listView = (ListView) view.findViewById(R.id.login);

       // ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
               // getActivity(),
              //  android.R.layout.simple_list_item_1,
              //  menuItems
      //  );

       // listView.setAdapter(listViewAdapter);

        //listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (position == 0) {
//                    Intent intent = new Intent(getActivity(), ProfileActivity.class);
//                    startActivity(intent);
//                } else if (position == 1) {
//                    Intent intent = new Intent(getActivity(), FriendListActivity.class);
//                    startActivity(intent);
//                } else if (position == 2) {
//                    Toast.makeText(getActivity(), "You clicked on the third item", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//        // Inflate the layout for this fragment
      return view;
  }
//
}
