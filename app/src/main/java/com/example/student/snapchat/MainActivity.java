package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "27F86E17-CEAA-C0A9-FF38-A6C61372F900";
    public static final String SECRET_KEY = "09CAC009-B657-6182-FFFB-9BB8DDC46A00";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      //  MainMenuFragment mainMenu = new MainMenuFragment();
       // getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == "") {
             MainMenuFragment mainMenu = new MainMenuFragment();
             getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        }else {
            CameraFragment Camera = new CameraFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, Camera).commit();
        }
    }
}
